#!../../bin/linux-x86_64/cmdexec

#- You may have to change cmdexec to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/cmdexec.dbd"
cmdexec_registerRecordDeviceDriver pdbbase

epicsEnvSet "EPICS_CA_MAX_ARRAY_BYTES", "1000000"

## Load record instances
dbLoadRecords "db/cmdexec.db", "user=hinkokocevar,INPUT_SIZE=500,OUTPUT_SIZE=50000"

#- Set this to see messages from mySub
# var mySubDebug 1
#- Set this to see messages from mySub2
# var mySubDebug2 1

#- Run this to trace the stages of iocInit
# traceIocInit

cd "${TOP}/iocBoot/${IOC}"
iocInit
