#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>

int mySubDebug;
int mySubDebug2;
pid_t child_pid = 0;

static long myAsubInit(aSubRecord *precord)
{
    return 0;
}

static long myAsubProcess(aSubRecord *precord)
{
    char *cmd = (char *)precord->a;
    if (mySubDebug) printf("%s: COMMAND: %s\n", __func__, cmd);
    // char *args = (char *)precord->b;
    // if (mySubDebug) printf("%s: ARGS: %s\n", __func__, args);

    // from waitpid man page example
    pid_t cpid, w;
    int wstatus;

    char *vala = (char *)precord->vala;
    memset(vala, 0, precord->nova);

    // use two pipe to communicate between parent and child
    int fd1[2];
  
    // https://www.geeksforgeeks.org/c-program-demonstrate-fork-and-pipe/
    if (pipe(fd1)==-1) { 
        printf("pipe() failed"); 
        sprintf(vala, "pipe() failed\n");
        return 0; 
    } 

    cpid = fork();
    if (cpid == -1) {
        printf("fork() failed"); 
        sprintf(vala, "fork() failed\n");
        return 0;
    }

   if (cpid == 0) {
        // code executed by child
        if (mySubDebug) printf("%s: child process %d ..\n", __func__, getpid());
        if (mySubDebug) printf("%s: NOVA = %d\n", __func__, precord->nova);

        FILE *fp;
        char partial_str[1035];
        long total_len = 0;
        long partial_len = 0;

        // close reading end of pipe
        close(fd1[0]);

        fp = popen(cmd, "r");
        if (fp) {
            while (fgets(partial_str, sizeof(partial_str), fp) != NULL) {
                partial_len = strlen(partial_str);
                if (mySubDebug) printf("%s: partial string [%ld]: '%s'", __func__, partial_len, partial_str);
                // check for overflow and clip it if needed
                if ((total_len + partial_len) >= (precord->nova - 1)) {
                    partial_len = precord->nova - total_len - 1;
                }
                // write string to pipe
                total_len += write(fd1[1], partial_str, partial_len);
                if (total_len >= (precord->nova - 1)) {
                    printf("%s: WARNING: output will be clipped to maximum %d characters!\n", __func__, precord->nova);
                    // no more writes to the pipe
                    break;
                }
            }
            pclose(fp);
        } else {
            printf("popen() failed\n");
            sprintf(partial_str, "popen() failed\n");
            total_len += write(fd1[1], partial_str, strlen(partial_str));
        }

        // Write \0 string and close writing end 
        total_len += write(fd1[1], '\0', 1); 
        close(fd1[1]); 
        if (mySubDebug) printf("%s: wrote total %ld characters to pipe..\n", __func__, total_len);

        // child must call _exit()!! .. not return!
         _exit(0);
   } else {
        // code executed by parent
        // only parent can set the child PID for myAsub2Process() function
        child_pid = cpid;

        // close writing end of pipe
        close(fd1[1]); 

        do {
            if (mySubDebug) printf("%s: parent process %d call waitpid() ..\n", __func__, getppid());
            w = waitpid(cpid, &wstatus, WUNTRACED | WCONTINUED);
            if (mySubDebug) printf("%s: parent process %d finished waitpid() ..\n", __func__, getppid());
            if (w == -1) {
                perror("waitpid");
                sprintf(vala, "waitpid() failed\n");
                return 0;
            }

            if (WIFEXITED(wstatus)) {
                if (mySubDebug) printf("exited, status=%d\n", WEXITSTATUS(wstatus));
            } else if (WIFSIGNALED(wstatus)) {
                if (mySubDebug) printf("killed by signal %d\n", WTERMSIG(wstatus));
            } else if (WIFSTOPPED(wstatus)) {
                if (mySubDebug) printf("stopped by signal %d\n", WSTOPSIG(wstatus));
            } else if (WIFCONTINUED(wstatus)) {
                if (mySubDebug) printf("continued\n");
            }
        } while (!WIFEXITED(wstatus) && !WIFSIGNALED(wstatus));

        if (mySubDebug) printf("%s: parent process %d finished ..\n", __func__, getppid());
       
        // read string from child and close reading end
        long n = read(fd1[0], vala, precord->nova);
        if (mySubDebug) printf("%s: saved %ld characters into vala..\n", __func__, n);
        close(fd1[0]); 
        // set the actual number of bytes on output
        precord->neva = n;
        // reset child PID
        child_pid = 0;
    }

    if (mySubDebug) printf("%s: return 0 - process output links\n", __func__);

    // process output links
    return 0;
}


static long myAsub2Init(aSubRecord *precord)
{
    return 0;
}

static long myAsub2Process(aSubRecord *precord)
{
    long *vala = (long *)precord->vala;
    vala[0] = child_pid;
    if (mySubDebug2) printf("%s: child process %d ..\n", __func__, child_pid);

    // process output links
    return 0;
}

epicsExportAddress(int, mySubDebug);
epicsExportAddress(int, mySubDebug2);
epicsRegisterFunction(myAsubInit);
epicsRegisterFunction(myAsubProcess);
epicsRegisterFunction(myAsub2Init);
epicsRegisterFunction(myAsub2Process);
